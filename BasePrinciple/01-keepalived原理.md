# Keepalived的原理

# 一、Keepalived简介

Keepalived的作用是检测服务器的状态，如果有一台web服务器宕机，或工作出现故障，Keepalived将检测到，并将有故障的服务器从系统中剔除，
同时使用其他服务器代替该服务器的工作，当服务器工作正常后Keepalived自动将服务器加入到服务器群中，
这些工作全部自动完成，不需要人工干涉，需要人工做的只是修复故障的服务器。

keepalived工作在IP/TCP协议栈的IP层，TCP层，及应用层，工作原理基于VRRP协议。

网络层(layer 3)：Keepalived会定期向服务器群中的服务器发送一个ICMP的数据包，（既我们平时用的Ping程序）,
如果发现某台服务的IP地址没有激活，Keepalived便报告这台服务器失效，并将它从服务器群中剔除。

传输层(layer 4)：Keepalived以TCP端口的状态来决定服务器工作正常与否，如web server的服务端口一般是80，
如果Keepalived检测到80端口没有启动，则Keepalived将把这台服务器从服务器群中剔除。

应用层(layer 5)：只要针对应用上的一些探测方式，如URL的get请求，或者对nginx脚本检测等；
可以根据用户自定义添加脚本针对特定的服务进行状态检测，当检测结果与用户设定不一致，则把这台服务器从服务器群中剔除。

# 二、VRRP协议与工作原理

![img.png](images/VRRP协议.png)

VRRP(Virtual Router Redundancy Protocol)虚拟路由冗余协议是一种容错的主备模式的协议，
当网络设备发生故障时，可以不影响主机之间通信情况下进行设备切换，并且相对用户时切换过程时透明的。

路由器开启VRRP功能后，会根据优先级确定自己在备份组中的角色。优先级高的路由器成为主用路由器，优先级低的成为备用路由器。
主用路由器拥有虚拟IP与虚拟MAC，实现各种网络功能，并发送VRRP通告报文，通知备用路由器组内的其他路由器自己工作正常；
备用路由器则启动定时器等待通告报文。抢占模式下，当备用路由器收到VRRP通告报文后，会将自己的优先级与通告报文中的优先级进行比较。
如果大于通告报文中的优先级，则成为主用路由器；否则将保持备用状态。非抢占模式下，只要主用路由器不发生故障，就算备用路由器的优先级再高，
也始终保持备用状态。如果备用路由器的定时器超时后仍未收到主用路由器发送来的VRRP通告报文，则认为主用路由器已经无法正常工作，
备份组内的路由器根据优先级选举出主用路由器。

- 一个VRRP路由器有唯一的标识：VRID，范围为0—255｡该路由器对外表现为唯一的虚拟MAC地址，地址的格式为00-00-5E-00-01-[VRID]。
- 同一台路由器可以加入多个备份组，在不同备份组中有不同的优先级，使得该路由器可以在一个备份组中作为主用路由器，在其他的备份组中作为备用路由器。
- 提供了两种安全认证措施：明文认证和IP头认证｡

# 二、VRRP选举机制

- 虚拟IP拥有者，如果某台路由器的IP地址与虚拟路由器的VIP地址一致，那么这台就会被选为主用路由器。
- 优先级较高者，如果没有虚拟IP拥有者，优先级数值大的路由器会被选举出，优先级范围0~255。
- IP地址较大者，如果优先级一样高，IP地址数值大的路由器会被选举出。


# 三、keepalived配置解析

**global_defs是对全局一些参数做出设置。**

```shell
global_defs {
   notification_email {
   # 接受邮件地址
     sysadmin@firewall.loc
   }
   notification_email_from master@keepalived.com
   # 邮件头地址
   smtp_server 192.168.200.1
   # 邮件服务smtp地址
   smtp_connect_timeout 30
   # smtp连接超时时间
   router_id LVS_DEVEL
   # 本地机器的id，可以为本地主机名
   vrrp_skip_check_adv_addr
   # 检查收到的通告中的所有地址会非常耗时，设置此参数就不会检查收到过的主路由器的通告
   vrrp_strict
   # 严格遵守VRRP协议，开启这个功能会在iptables中添加下面一条规则，导致VIP ping不通
   # Chain INPUT (policy ACCEPT)
   # target     prot opt source               destination
   # DROP       all  --  0.0.0.0/0            172.30.100.200
   vrrp_garp_interval 0
   # 一个网卡上每组gratuitous arp消息之间的延迟时间，默认为0
   vrrp_gna_interval 0
   # 一个网卡上每组na消息之间的延迟时间，默认为0
   vrrp_iptables
   # 设置此项则不会开启任何iptables规则
   vrrp_mcast_group4 224.0.0.18
   # 主备间通告状态信息的组播地址，通常为224网段
}
```

**vrrp_script只是定义一个对集群服务的健康状态检测脚本，**
并根据监控的结果状态能实现优先动态调整；这种方式在做高可用nginx或者haproxy时需要用到。

```shell
vrrp_script chk_nginx {
    script
    # 添加要定期执行的检测脚本，它的退出代码将被记录在监视它的所有VRRP实例中
    interval 2
    # 两次脚本调用的间隔，默认为1秒
    weight -20
    # 如果检测条件成立，权重-20
}
```

**vrrp_instance是虚拟路由实例的配置段，**
用来定义当前keepalived节点的运行角色为主或是备，定义当前节点的优先级，设置虚拟IP等信息。

```shell
vrrp_instance VI_1 {
# 定义虚拟路由器，实例名称VI_1主备必须相同
    state MASTER
    # 定义此节点为MASTER
    interface eth0
    # 绑定为当前虚拟路由器使用的物理接口
    virtual_router_id 51
    # 设置虚拟路由id，范围0~255，主备必须相同
    priority 100
    # 设置此节点优先级
    advert_int 1
    # 设置主备节点间vrrp通告状态的时间间隔<br><br>    nopreempt
    authentication {
    # 设置主备间验证方式
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
    # 设置虚拟路由VIP
        172.30.200.1
    }
    track_script {
    # 添加监控脚本，脚本名为vrrp_script中定义的脚本
        chk_nginx
    }
}
```

**virtual_server中定义了LVS工作模式及调度算法等信息，real_server则是定义后端的真实服务器。**

```shell
virtual_server 172.30.200.1 80 {
# 定义虚拟主机IP与端口
    delay_loop 6
    # 健康状态检测时间间隔
    lb_algo rr
    # lvs调度算法
    lb_kind NAT
    # lvs工作类型
    persistence_timeout 50
    # lvs持久连接超时时间
    protocol TCP
    # 定义服务协议
    sorry_server 172.30.10.14 80
    # 当后端所有主机不可达时，就会被定义这台主机，只是为了展示sorry页面
    real_server 172.30.10.12 80 {
    # 配置后端主机
        weight 1
        # 调度权重
        HTTP_GET {
        # 应用层检测
            url {
              path /testurl/test.jsp
              # 定义被检测的URL
              status_code 200
              # 判断上述URL健康状态的响应码
              digest 640205b7b0fc66c1ea91c463fac6334d
              # 判断上述URL健康状态的内容的hash码
            }
            connect_timeout 3
            # 检测连接的超时时间
            nb_get_retry 3
            # 重试次数
            delay_before_retry 3
            # 重试前等待时间
        }
        TCP_CHECK {
             connect_ip 172.30.10.12
             # 选择要连接的IP地址。默认值为realserver IP
             connect_port 80
             # 选择要连接的端口。默认值为realserver PORT
             bindto 172.30.10.11
             # 用于发起连接的可选源地址
             bind_port 10000
             # 用于发起连接的可选源端口
             connect_timeout 3
             # 连接超时时间
        }
    }
    real_server 172.30.10.12 80 {
        weight 1
        HTTP_GET {
            url {
              path /testurl/test.jsp
              digest 640205b7b0fc66c1ea91c463fac6334c
            }
            connect_timeout 3
            nb_get_retry 3
            delay_before_retry 3
        }
    }
}
```

# 四、keepalived被限制使用的场景

一般情况下，我们喜欢使用keepalived来实现主备切换，从而实现服务高可用。但是一般是在我们自己的网络内使用，
当keepalived在公有网络架构上，往往无法直接使用。公有网络一般在网关设备上会做流量抑制。目的1、防止过多泛洪，使得网络压力巨大，引起全局颠簸。
2、防止有目的性的防洪攻击。在公有网络上，交换机一般会做以下限制:

```shell
1、开启端口广播风暴抑制功能，并设置广播风暴抑制阈值。

broadcast-suppression { ratio | pps max-pps | kbps max-kbps }

2、开启端口组播风暴抑制功能，并设置组播风暴抑制阈值。

multicast-suppression { ratio | pps max-pps | kbps max-kbps }

3、开启端口未知单播风暴抑制功能，并设置未知单播风暴抑制阈值。

unicast-suppression { ratio | pps max-pps | kbps max-kbps }
```

**网关设备上的流量抑制导致keepalived受限分析：**

当在同一个局域网部署了多级keepalived服器对时，而又未使用专门的心跳线通信时，可能会发生高可用接管的严重故障问题。
keepalived 高可用功能是通过VRRP 协议实现的，VRRP 协议默认通过IP 多播的形式实现高可用对之间的通信，
如果同一个局域网内存在多组keepalived服务器对，就会造成IP多播地址冲突问题，导致接管错乱，不同组的keepalived 都会使用默认的224.0.0.18 作为多播地址。

还要分析垮不垮交换机，以及交换机是设置交换机内可播，交换机间不可播，还是交换机内不可播，且交换机之间也不可播。
一般情况下是均不可播，以避免未知风暴。有时候物理网络架构使用了VLAN划分，那也可能是开启可播的。要具体分析。

交换机内不可播，且交换机之间也不可播：
- 此时keepalived无论何种模式均无法使用

交换机内可播，且交换机之间也不可播，机器在不同交换机下：

- 此时keepalived无论何种模式均无法使用

交换机内可播，交换机间不可播，机器在同一交换机下：

- keepalived使用单播模式，且交换机只开启了组播抑制：正常使用 
- keepalived使用组播模式，且交换机开启了组播抑制：无法使用
- keepalived使用单播模式，且交换机开启了未知单播抑制：无法使用

# 五、keepalived的脑裂问题

曾经在做keepalived+Nginx主备架构的环境时，当重启了备用机器后，发现两台机器都拿到了VIP。
这也就是意味着出现了keepalived的脑裂现象，检查了两台主机的网络连通状态，发现网络是好的。然后在备机上抓包：

# tcpdump -i eth0|grep VRRP

```shell
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode

listening on eth0, link-type EN10MB (Ethernet), capture size 65535 bytes

22:10:17.146322 IP 192.168.1.54 > vrrp.mcast.net: VRRPv2, Advertisement, vrid 51, prio 160, authtype simple, intvl 1s, length 20

22:10:17.146577 IP 192.168.1.96 > vrrp.mcast.net: VRRPv2, Advertisement, vrid 51, prio 50, authtype simple, intvl 1s, length 20

22:10:17.146972 IP 192.168.1.54 > vrrp.mcast.net: VRRPv2, Advertisement, vrid 51, prio 160, authtype simple, intvl 1s, length 20

22:10:18.147136 IP 192.168.1.96 > vrrp.mcast.net: VRRPv2, Advertisement, vrid 51, prio 50, authtype simple, intvl 1s, length 20
```

抓包发现备机能接收到master发过来的VRRP广播，那为什么还会有脑裂现象？ 接着发现iptables开启着，检查了防火墙配置。
发现系统不接收VRRP协议。于是修改iptables，添加允许系统接收VRRP协议的配置:

```shell
-A INPUT -i lo -j ACCEPT
-----------------------------------------------------------------------------------------
自己添加了下面的iptables规则：
-A INPUT -s 192.168.1.0/24 -d 224.0.0.18 -j ACCEPT       #允许组播地址通信
-A INPUT -s 192.168.1.0/24 -p vrrp -j ACCEPT             #允许VRRP（虚拟路由器冗余协）通信
-----------------------------------------------------------------------------------------
```

最后重启iptables，发现备机上的VIP没了。 虽然问题解决了，但备机明明能抓到master发来的VRRP广播包，却无法改变自身状态。
只能说明网卡接收到数据包是在iptables处理数据包之前。

## 5.1 预防keepalived脑裂问题

可以采用第三方仲裁的方法。由于keepalived体系中主备两台机器所处的状态与对方有关。
如果主备机器之间的通信出了网题，就会发生脑裂，此时keepalived体系中会出现双主的情况，产生资源竞争。

一般可以引入仲裁来解决这个问题，即每个节点必须判断自身的状态。最简单的一种操作方法是，在主备的keepalived的配置文件中增加check配置，
服务器周期性地ping一下网关，如果ping不通则认为自身有问题 。最容易的是借助keepalived提供的vrrp_script及track_script实现

#vim /etc/keepalived/keepalived.conf

```shell
......
vrrp_script check_local {
script "/root/check_gateway.sh"
interval 5
}

track_script {    
check_local                  
}


脚本内容:

# cat /root/check_gateway.sh

#!/bin/sh

VIP=$1

GATEWAY=192.168.1.1

/sbin/arping -I em1 -c 5 -s $VIP $GATEWAY &>/dev/null

check_gateway.sh 就是我们的仲裁逻辑，发现ping不通网关，则关闭keepalived。
```

# 六、Keepalived限制死20个VIP问题

由于业务需要，在部署LVS+Keepalived时，公司申请了一个C网段，让keepalived绑定254个VIP地址。
满满的以为没有一点问题，但却遇到了keepalived限制死20 VIP的问题。刚开始一无所措，但经过思考，以及请教别人，得到了很多种思路，
虽然有些思路并没解决问题，但思路本身就是很重要的，因为以后难免会用同样思路解决其他的问题。

```shell
# tail /var/log/syslog

Nov 12 18:15:34 Telcom-DG-WY-LVS-13-130 Keepalived_vrrp[27984]:   => Declare others VIPs into the excludedvip block

Nov 12 18:15:34 Telcom-DG-WY-LVS-13-130 Keepalived_vrrp[27984]: VRRP_Instance(VI_1) trunc to the first 20 VIPs.

Nov 12 18:15:34 Telcom-DG-WY-LVS-13-130 Keepalived_vrrp[27984]:   => Declare others VIPs into the excludedvip block

Nov 12 18:15:34 Telcom-DG-WY-LVS-13-130 Keepalived_vrrp[27984]:VRRP_Instance(VI_1) trunc to the first 20 VIPs.
```

使用keepalived的脚本触发功能：LVS最好还是和keepalived用的好，于是提出keepalived的脚本触发功能。即keepalived自身虽然只能配置20个VIP，
但是在一个实例变为主或从时，能够触发脚本，所以：当变为MASTER时，调用添加VIP脚本，变为BACKUP时，调用删除VIP脚本。经过测试，简单可靠实在。

```shell
$ cat/etc/keepalived/keepalived.conf|grep notify

notify_master   /scripts/add_ip_1_130.sh    #添加130个VIP的脚本

notify_backup   /scripts/del_ip_1_130.sh    #删除130个VIP的脚本
```

# 七、keepalived无法绑定虚ip

![img.png](images/keepalived无法绑定虚ip.png)

报错信息为(VI_HA): ip address associated with VRID 123 not present in MASTER advert : 172.103.201.120，意思为无法绑定虚IP，
经过网上查找原因，有两种可能性：

- 主备服务器时间不匹配，需要修改一致。
- 虚拟路由ID在局域网中冲突。

# 博文参考
https://keepalived.org/